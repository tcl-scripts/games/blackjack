#  _     _                 _ _     _
# | |   | |               | | |   (_)
# | |__ | | ___   ___   __| | |    _ _ __   ___ _ __
# | '_ \| |/ _ \ / _ \ / _` | |   | | '_ \ / _ \ '__|
# | |_) | | (_) | (_) | (_| | |___| | | | |  __/ |
# |_.__/|_|\___/ \___/ \__,_\_____/_|_| |_|\___|_|
#
# Blackjack Script by bloodLiner
#
#
#
# # author: bloodLiner
# # version: 1.2
# # web: http://www.bloodliner.de
# # irc: #bloodLiner @ QuakeNet
# # contact: me@bloodliner.de
#
# # Installation:
#	Upload the script into the scripts folder of your eggdrop
#	and insert 'source scripts/blackjack.tcl' to your eggdrop.conf
#
# # Changelog:
#
#	# 11.12.2006 - v1.0: Public Release!
#	# 12.12.2006 - v1.1: Fixed a bug, that came up at the end of the game. A variable was missing.
#   # 05.06.2020 - v1.2: corrections by CrazyCat 
#
# # Usage:
#	?blackjack 			- Start a game
#	?blackjack on 		- Turn on Blackjack in the Channel
#	?blackjack off		- Turn off Blackjack in the Channel
#	?blackjack stats	- Get Channel statistics for Blackjack
#	?blackjack version	- Shows the Blackjack script version
#	?join 				- Join a game
#	?card 				- Get a card
#	?enough				- Finish
#	?stop				- Just for Bot Owners, stop a game if it freezes by a bug
#
#
#
# # Copyright
#
# Copyright (C) 2006  Michael 'bloodLiner' Gecht
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# # Configuration:
#
# Game trigger, strandard is ?blackjack
set ::blackjack(trigger) "?"

# Floodtime, 300 seconds = 5 minutes
set ::blackjack(flood) "30"

#
# # DON'T CHANGE ANYTHING BEYOND THIS LINE, UNTIL YOU KNOW WHAT YOU ARE DOING!

set bj(author) 		"bloodLiner"
set bj(web) 		"http://www.bloodliner.de"
set bj(name) 		"Blackjack Script"
set bj(version) 	"v1.2"

setudef 	flag 	blackjack
setudef 	str  	blackjackc

bind pub 	* 	$::blackjack(trigger)blackjack 	game:blackjack
bind pub 	* 	$::blackjack(trigger)entrar 	blackjack:join
bind pub 	* 	$::blackjack(trigger)carta 		blackjack:karte
bind pub 	* 	$::blackjack(trigger)basta	 	blackjack:genug
bind pub    M   $::blackjack(trigger)stop 		blackjack:stop
bind pub - !blackjack game:blackjack

# sendmsg proc by ircscript.de - R.I.P. #ircscript
proc sendmsg {target command message} {
	if {![string match "#*" $target]} {
		putquick "notice $target « \00304$command\017» « \00304$message\017 »"
	} else {
		if {[string match "*c*" [getchanmode $target]]} {
			putquick "privmsg $target :« \00304$command\017 » « \00304$message\017 »"
		} else {
			putquick "privmsg $target :« \00304$command\017 » « \00304$message\017 »"
		}
	}
}
#<+Raspdrop> « Blackjack » « CrazyCat Se ha plantado, [Guru] Eres el siguiente
# string2pattern proc by CyBex - tclhelp.net
proc str2pat {string} {
	return [string map [list \\ \\\\ \[ \\\[ \] \\\] ] $string]
}

proc game:blackjack {nick host hand chan arg} {
	switch -exact -- [string tolower [lindex [split $arg] 0]] {
		"on" {
			if {![matchattr $hand |+M $chan]} {
				return 0
			}
			if {[channel get $chan "blackjack"]} {
				putserv "notice $nick :El juego \00306Blackjack\017 ya está habilitado en \00306$chan\017"
				return 0
			} elseif {![channel get $chan "blackjack"]} {
				set ::blackjack(player,$chan) ""
				channel set $chan +blackjack
				putserv "notice $nick :\00306Blackjack \017Habilitado exitosamente en \00306$chan\017."
			}
		}
		"off" {
			if {![matchattr $hand |+M $chan]} {
				return 0
			}
			if {![channel get $chan "blackjack"]} {
				putserv "notice $nick :El \00306Blackjack\017 ya esta deshabilitado en \00306$chan\017."
				return 0
			} elseif {[channel get $chan "blackjack"]} {
				channel set $chan -blackjack
				putserv "notice $nick :\00306Blackjack\017 Deshabilitado exitosamente en \00306$chan\017."
			}
		}
		"stats" {
			if {[info exists ::blackjack(flood,count,$chan)] && [expr {[unixtime] - $::blackjack(flood,count,$chan)}] < 300} {
				return 0
			} else {
				if {[channel get $chan "blackjackc"] == ""} {
					sendmsg $chan Blackjack "Nunca he visto ningún juego en \00306$chan\017!"
				} elseif {[channel get $chan "blackjackc"] == "1"} {
					sendmsg $chan Blackjack "He visto exactamente un juego en \00306$chan\017!"
				} else {
					sendmsg $chan Blackjack "He visto \00306[channel get $chan "blackjackc"]\017 juegos en \00306$chan\017!"
				}
				set ::blackjack(flood,count,$chan) [unixtime]
				utimer 30 [list unset ::blackjack(flood,count,$chan)]
			}
		}
		"version" {
			global bj
			if {[info exists ::blackjack(flood,version,$chan)] && [expr {[unixtime] - $::blackjack(flood,version,$chan)}] < 300} {
			return 0
			} else {
				sendmsg $chan Blackjack "I'm using the $bj(name) $bj(version) by $bj(author) - $bj(web)"
				set ::blackjack(flood,version,$chan) [unixtime]
				utimer 300 [list unset ::blackjack(flood,version,$chan)]
			}
		}
		"" {
			if {![channel get $chan "blackjack"]} {
				return 0
			} elseif {[info exists ::blackjack(flood,$chan)] && [expr {[unixtime] - $::blackjack(flood,$chan)}] < $::blackjack(flood)} {
				return 0
			} else {
				if {[info exists ::blackjack(request,$chan)] == "1" || [info exists ::blackjack(started,$chan)] == "1"} {
					puthelp "notice $nick :Ya hay un juego en curso en el canal \00306 $chan"
					return 0
				} else {
					set ::blackjack(request,$chan) "1"
				}
			}
			if {$::blackjack(request,$chan) == "1"} {
				set ::blackjack(player,$chan) "[str2pat $nick]"
				set ::blackjack(active,$chan) "0"
				sendmsg $chan Blackjack "El juego comenzará en los próximos 30 segundos! Escriba\00306 $::blackjack(trigger)entrar \00302para unirse al juego!"
				utimer 30 [list blackjack:expire $chan]
				return
			}
		}
	}
}

proc blackjack:join {nick host hand chan arg} {
	if {![channel get $chan "blackjack"]} {
		return 0
	} elseif {[info exists ::blackjack(request,$chan)] == "0"} {
		return 0
	} elseif {[llength $::blackjack(player,$chan)] == 7} {
		puthelp "notice $nick :Lo siento El juego ya esta completo"
		return 0
	}
	if {[lsearch $::blackjack(player,$chan) [str2pat $nick]] == "-1"} {
		lappend ::blackjack(player,$chan) $nick 
		puthelp "privmsg $chan \00304$nick: \00302Se ha unido al juego"
	} else {
		puthelp "notice $nick :Ya estas unido al juego en \00306$chan"
	}
}

proc blackjack:expire {chan} {
	if {[info exists ::blackjack(request,$chan)]&&[llength $::blackjack(player,$chan)] < 2} {
		sendmsg $chan Blackjack "Se acabaron los 30 segundos y nadie quiere jugar.... \00304cagones \00302:P"
		unset ::blackjack(player,$chan)
		unset ::blackjack(request,$chan)
	} else {
		unset ::blackjack(request,$chan)
		set ::blackjack(started,$chan) "1"
		foreach player $::blackjack(player,$chan) {
			set ::blackjack(gesamt,wert,$chan,[getchanhost $player]) "0"
			set ::blackjack(gesamt,karten,$chan,[getchanhost $player]) ""
		}
		set ::blackjack(stapel,picas,$chan) "as 2 3 4 5 6 7 8 9 10 Sota Reina Rey"
		set ::blackjack(stapel,trebol,$chan) "as 2 3 4 5 6 7 8 9 10 Sota Reina Rey"
		set ::blackjack(stapel,Corazones,$chan) "as 2 3 4 5 6 7 8 9 10 Sota Reina Rey"
		set ::blackjack(stapel,Diamantes,$chan) "as 2 3 4 5 6 7 8 9 10 Sota Reina Rey"
		set ::blackjack(stapel,alle,$chan) "picas trebol Corazones Diamantes"
		sendmsg $chan Blackjack "El juego ha comenzado entre los jugadores \00304[join $::blackjack(player,$chan) ", "]\017. Consigue las cartas usando \00306$::blackjack(trigger)carta\017 Cuando tengas suficiente utiliza \00306$::blackjack(trigger)basta\017 Comienza \00304[lindex $::blackjack(player,$chan) 0]\017"
		set ::blackjack(idletimer,$chan) [utimer 60 [list blackjack:idle [lindex $::blackjack(player,$chan) $::blackjack(active,$chan)] $chan]]
	}
}

proc blackjack:karte {nick host hand chan arg} {
	if {![channel get $chan "blackjack"]} {
		return 0
	} elseif {![info exists ::blackjack(started,$chan)]} {
		return 0
	} elseif {$nick != [lindex $::blackjack(player,$chan) $::blackjack(active,$chan)]} {
		return 0
	}

	if {[info exists ::blackjack(idletimer,$chan)]} {
		killutimer $::blackjack(idletimer,$chan)
		unset ::blackjack(idletimer,$chan)
	}

	foreach stapel $::blackjack(stapel,alle,$chan) {
		if {[llength $::blackjack(stapel,$stapel,$chan)] < 1} {
			set ::blackjack(stapel,alle,$chan) "[lrange $::blackjack(stapel,alle,$chan) 0 [expr {[lsearch -exact $::blackjack(stapel,alle,$chan) $stapel] - 1}]] [lrange $::blackjack(stapel,alle,$chan) [expr {[lsearch -exact $::blackjack(stapel,alle,$chan) $stapel] + 1}] end]"
			set ::blackjack(stapel,$chan) "[rand [llength $::blackjack(stapel,alle,$chan)]]"
			set ::blackjack(stapelw,$chan) "[lindex $::blackjack(stapel,alle,$chan) $::blackjack(stapel,$chan)]"
			set ::blackjack(karte,$chan) "[rand [llength $::blackjack(stapel,$::blackjack(stapelw,$chan),$chan)]]"
			set ::blackjack(wert,$chan) "[lindex $::blackjack(stapel,[lindex $::blackjack(stapel,alle,$chan) $::blackjack(stapel,$chan)],$chan) $::blackjack(karte,$chan)]"
		} else {
			set ::blackjack(stapel,$chan) "[rand [llength $::blackjack(stapel,alle,$chan)]]"
			set ::blackjack(stapelw,$chan) "[lindex $::blackjack(stapel,alle,$chan) $::blackjack(stapel,$chan)]"
			set ::blackjack(karte,$chan) "[rand [llength $::blackjack(stapel,$::blackjack(stapelw,$chan),$chan)]]"
			set ::blackjack(wert,$chan) "[lindex $::blackjack(stapel,[lindex $::blackjack(stapel,alle,$chan) $::blackjack(stapel,$chan)],$chan) $::blackjack(karte,$chan)]"
		}
	}
	if {$::blackjack(stapelw,$chan) == "picas" || $::blackjack(stapelw,$chan) == "trebol"} {
		set blackjack(farbe,$chan) "\0031"
	} elseif {$::blackjack(stapelw,$chan) == "Corazones" || $::blackjack(stapelw,$chan) == "Diamantes"} {
		set blackjack(farbe,$chan) "\0034"
	}

	if {$::blackjack(gesamt,wert,$chan,$host) == 21 || $::blackjack(gesamt,wert,$chan,$host) > 21} {
		puthelp "notice $nick :Tu valor ya es \00306$::blackjack(gesamt,wert,$chan,$host)\017 ! ahora escribe \00306$::blackjack(trigger)basta\017"
		return 0
	} elseif {$::blackjack(wert,$chan) == "Jack" || $::blackjack(wert,$chan) == "Reina" ||  $::blackjack(wert,$chan) == "Rey"} {
		set ::blackjack(gesamt,wert,$chan,$host) "[expr {$::blackjack(gesamt,wert,$chan,$host) + 10}]"
	} elseif {$::blackjack(wert,$chan) == "as"} {
		if {[expr {$::blackjack(gesamt,wert,$chan,$host) + 11}] > 21} {
			set ::blackjack(gesamt,wert,$chan,$host) "[expr {$::blackjack(gesamt,wert,$chan,$host) + 1}]"
		} else {
			set ::blackjack(gesamt,wert,$chan,$host) "[expr {$::blackjack(gesamt,wert,$chan,$host) + 11}]"
		}
	} else {
		set ::blackjack(gesamt,wert,$chan,$host) "[expr {$::blackjack(gesamt,wert,$chan,$host) + $::blackjack(wert,$chan)}]"
	}

	set ::blackjack(gesamt,karten,$chan,$host) "$::blackjack(gesamt,karten,$chan,$host) $blackjack(farbe,$chan)$::blackjack(stapelw,$chan) $::blackjack(wert,$chan)\003"

	putquick "notice $nick :Tus Cartas \00306:$::blackjack(gesamt,karten,$chan,$host)\017 - Valor Total : \00306$::blackjack(gesamt,wert,$chan,$host)\017"
	set ::blackjack(stapel,[lindex $::blackjack(stapel,alle,$chan) $::blackjack(stapel,$chan)],$chan) "[lrange $::blackjack(stapel,[lindex $::blackjack(stapel,alle,$chan) $::blackjack(stapel,$chan)],$chan) 0 [expr {$::blackjack(karte,$chan)-1}]] [lrange $::blackjack(stapel,[lindex $::blackjack(stapel,alle,$chan) $::blackjack(stapel,$chan)],$chan) [expr {$::blackjack(karte,$chan)+1}] end]"
}

proc blackjack:idle {nick chan} {
	sendmsg $chan Blackjack "\00306$nick\017 Parece estar todo cagado...."
	unset ::blackjack(idletimer,$chan)
	blackjack:genug $nick [getchanhost $nick] [nick2hand $nick] $chan keyed
}

proc blackjack:kick {nick chan} {
	set player $::blackjack(player,$chan)
	set ::blackjack(player,$chan) ""
	foreach players $player {
		if {$players != $nick} {
			lappend ::blackjack(player,$chan) $players
		} else {
			continue;
		}
	}
	unset player
}

proc blackjack:genug {nick host hand chan arg} {
	if {![channel get $chan "blackjack"]} {
		return 0
	} elseif {![info exists ::blackjack(started,$chan)]} {
		return 0
	}
	if {$nick != [lindex $::blackjack(player,$chan) $::blackjack(active,$chan)]} {
		return 0
	}
	if {$::blackjack(gesamt,wert,$chan,$host) == 0 && [llength $::blackjack(gesamt,karten,$chan,$host)] == 0 && $arg != "keyed"} {
		puthelp "notice $nick :Usted debe recibir al menos una carta antes de poder usar \00306$::blackjack(trigger)basta\017"
		return 0
	} elseif {$::blackjack(gesamt,wert,$chan,[getchanhost [lindex $::blackjack(player,$chan) $::blackjack(active,$chan)]]) > 21 || $::blackjack(gesamt,wert,$chan,[getchanhost [lindex $::blackjack(player,$chan) $::blackjack(active,$chan)]]) == "0" && $arg == "keyed"} {
		blackjack:kick [lindex $::blackjack(player,$chan) $::blackjack(active,$chan)] $chan
	} else {
		incr ::blackjack(active,$chan)
	}
	if {[expr {[llength $::blackjack(player,$chan)]-1}] < $::blackjack(active,$chan)} {
		if {[llength $::blackjack(player,$chan)] < 1} {
			sendmsg $chan Blackjack "No hubo ganadores"
		} else {
			set ::blackjack(winner,$chan,check) "$::blackjack(gesamt,wert,$chan,[getchanhost [lindex $::blackjack(player,$chan) 0]])"
			set ::blackjack(winner,$chan) "[lindex $::blackjack(player,$chan) 0]"
			set ::blackjack(winner,$chan,zahl) "1"
			foreach player $::blackjack(player,$chan) {
				if {$::blackjack(winner,$chan) == $player} {
					continue;
				} elseif {$::blackjack(gesamt,wert,$chan,[getchanhost $player]) > 21} {
					continue;
				} elseif {$::blackjack(gesamt,wert,$chan,[getchanhost $player]) > $::blackjack(winner,$chan,check)} {
					set ::blackjack(winner,$chan) "$player"
					set ::blackjack(winner,$chan,check) "$::blackjack(gesamt,wert,$chan,[getchanhost $player])"
					continue;
				} elseif {$::blackjack(gesamt,wert,$chan,[getchanhost $player]) == $::blackjack(winner,$chan,check)} {
					lappend ::blackjack(winner,$chan) "$player"
					continue;
				}
			}
			if {[llength $::blackjack(winner,$chan)] > 1} {
				set ::blackjack(player,$chan) "$::blackjack(winner,$chan)"
				foreach player $::blackjack(player,$chan) {
					set ::blackjack(gesamt,wert,$chan,[getchanhost $player $chan]) "0"
					set ::blackjack(gesamt,karten,$chan,[getchanhost $player $chan]) ""
				}
				set ::blackjack(stapel,picas,$chan) "As 2 3 4 5 6 7 8 9 10 Sota Reina Rey"
				set ::blackjack(stapel,trebol,$chan) "As 2 3 4 5 6 7 8 9 10 Sota Reina Rey"
				set ::blackjack(stapel,Corazones,$chan) "As 2 3 4 5 6 7 8 9 10 Sota Reina Rey"
				set ::blackjack(stapel,Diamantes,$chan) "As 2 3 4 5 6 7 8 9 10 Sota Reina Rey"
				set ::blackjack(stapel,alle,$chan) "picas trebol Corazones Diamantes"
				set ::blackjack(active,$chan) "0"
				sendmsg $chan Blackjack "Los jugadores \00304 [join $::blackjack(player,$chan) ", "] Tienen los mismos valores: \00306$::blackjack(winner,$chan,check)\017. Es por eso que comenzara una nueva partida entre ellos \00304[lindex $::blackjack(player,$chan) 0]\017 Va primero."
				set ::blackjack(idletimer,$chan) [utimer 60 [list blackjack:idle [lindex $::blackjack(player,$chan) 0] $chan]]
				return 0
			} else {
				sendmsg $chan Blackjack "El ganador es \00306$::blackjack(winner,$chan)\017 con un valor de \00303$::blackjack(gesamt,wert,$chan,[getchanhost $::blackjack(winner,$chan) $chan])\017!  \00302:::\002ATENCION\002:::\00302 esperar \0030630\017seg. para iniciar un nuevo juego"
			}
			unset ::blackjack(winner,$chan)
			unset ::blackjack(winner,$chan,zahl)
			unset ::blackjack(winner,$chan,check)
		}
		if {[channel get $chan "blackjackc"] == ""} {
			set bjcount "0"
		} else {
		set bjcount "[channel get $chan blackjackc]"
		}
		incr bjcount
		channel set $chan blackjackc "$bjcount"
		set ::blackjack(flood,$chan) [unixtime]
		utimer 300 [list unset ::blackjack(flood,$chan)]
		foreach player $::blackjack(player,$chan) {
			unset ::blackjack(gesamt,wert,$chan,[getchanhost $player $chan])
		}
		unset ::blackjack(player,$chan)
		unset ::blackjack(started,$chan)
		unset ::blackjack(stapel,picas,$chan)
		unset ::blackjack(stapel,trebol,$chan)
		unset ::blackjack(stapel,Corazones,$chan)
		unset ::blackjack(stapel,Diamantes,$chan)
		unset ::blackjack(stapel,alle,$chan)
		unset ::blackjack(stapel,$chan)
		unset ::blackjack(stapelw,$chan)
		unset ::blackjack(karte,$chan)
		unset ::blackjack(wert,$chan)
		return 0
	} else {
		sendmsg $chan Blackjack "\00306$nick\017 Se ha plantado, \00306[lindex $::blackjack(player,$chan) $::blackjack(active,$chan)]\017 Eres el siguiente"
		set ::blackjack(idletimer,$chan) [utimer 60 [list blackjack:idle [lindex $::blackjack(player,$chan) $::blackjack(active,$chan)] $chan]]
	}
}

proc blackjack:stop {nick host hand chan arg} {
	if {[info exists ::blackjack(request,$chan)]} {
		unset ::blackjack(request,$chan)
		putquick "privmsg $chan :\00306Hecho!\017 El juego \00304Blackjack\017 se ha restablecido en \00306$chan\017!"
	}
	if {[info exists ::blackjack(started,$chan)]} {
		unset ::blackjack(started,$chan)
		putquick "privmsg $chan :\00306Hecho!\017 El juego \00304Blackjack\017 se ha restablecido en \00306$chan\017!"
	}
}

putlog "\033\[01\;31mJuego Blackjack \033\[01\;32mCargado correctamente\033\[\;0m"

# EOF